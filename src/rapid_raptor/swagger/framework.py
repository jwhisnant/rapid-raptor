"""
This is a script that has definitions
The intent is that this live in some place for all projects ...
"""

import logging
from functools import wraps

import falcon
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from falcon_apispec import FalconPlugin
from marshmallow import Schema, fields

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# XXX TODO: refactor this ... 2020/05/22  ()


class TODO(Schema):
    """ TODO """

    todo = fields.Str(required=True, example='TODO')


class CorrelationIdSchema(Schema):
    correlation_id = fields.Str(
        required=True, example='da7cdff7-4cad-4ab8-8a8e-6a66072c60ee'
    )


class MetaSchema(Schema):
    svrStatus = fields.Int(required=True, example=0, description='status code')
    svrMessage = fields.Str(required=True, example='', description='message')
    svrCtrlCode = fields.Str(
        required=True,
        example='da7cdff7',
        description='unique code (first 8 digits of correlation-id)',
    )
    svrCtrlName = fields.Str(
        required=True, example='errSuccess', description='key for content'
    )

    # XXX TODO: these should be date and time fields 2019-08-26  (jwhisnant)
    svrLsDate = fields.Str(required=True, example='01/01/2019', description='date')
    svrLsTime = fields.Str(required=True, example='12:01:01pm', description='time')


class ErrorSchema(Schema):
    """Http Error block  """

    status = fields.Str(required=True)
    code = fields.Str(required=True)
    cause = fields.Str(required=True)
    message = fields.Str(required=True)


class Error400Schema(ErrorSchema):
    status = fields.Str(required=True, example='400 Bad Request')
    code = fields.Str(required=True, example='400 error')
    cause = fields.Str(required=True, example='400 error')
    message = fields.Str(required=True, example='400 error')


class Error404Schema(ErrorSchema):
    status = fields.Str(required=True, example='404 Not Found')
    code = fields.Str(required=True, example='404 error')
    cause = fields.Str(required=True, example='404 error')
    message = fields.Str(required=True, example='404 error')


class Error422Schema(ErrorSchema):
    status = fields.Str(required=True, example='422 Unprocessable Entity')
    code = fields.Str(required=True, example='422 error')
    cause = fields.Str(required=True, example='422 error')
    message = fields.Str(required=True, example='422 error')


class Error500Schema(ErrorSchema):
    status = fields.Str(required=True, example='500')
    code = fields.Str(required=True, example='Unhandled Error')
    cause = fields.Str(required=True, example='An unexpected error occurred.')
    message = fields.Str(required=True, example='Contact PyMSA team')


class CF400Schema(Schema):
    correlation_id = fields.Nested(CorrelationIdSchema, many=False)
    meta = fields.Nested(MetaSchema, many=False)
    errors = fields.Nested(Error400Schema, many=True)


class CF404Schema(Schema):
    correlation_id = fields.Nested(CorrelationIdSchema, many=False)
    meta = fields.Nested(MetaSchema, many=False)
    errors = fields.Nested(Error404Schema, many=True)


class CF422Schema(Schema):
    correlation_id = fields.Nested(CorrelationIdSchema, many=False)
    meta = fields.Nested(MetaSchema, many=False)
    errors = fields.Nested(Error422Schema, many=True)


class CF500Schema(Schema):
    correlation_id = fields.Nested(CorrelationIdSchema, many=False)
    meta = fields.Nested(MetaSchema, many=False)
    errors = fields.Nested(Error500Schema, many=True)


def cf_writer(*args, **kwargs):
    """ given a resource, return a doc-string for each of the
    methods with the standard doc strings to build a swagger doc
    """

    def wrapper(cls):

        get = getattr(cls, 'on_get', None)
        if get:
            out = cf_get_writer(cls, get)
            cls.on_get.__doc__ = out

        post = getattr(cls, 'on_post', None)
        if post:
            out = cf_post_writer(cls, post)
            cls.on_post.__doc__ = out

        put = getattr(cls, 'on_put', None)
        if put:
            out = cf_put_writer(cls, put)
            cls.on_put.__doc__ = out

        delete = getattr(cls, 'on_delete', None)
        if delete:
            out = cf_delete_writer(cls, delete)
            cls.on_delete.__doc__ = out

        return cls

    return wrapper


def cf_get_writer(cls, get):
    try:
        name = cls.response_schema.__class__.__name__
    except AttributeError as err:
        logger.debug(err)
        return get.__doc__

    res = name.lower().replace('resource', '')

    out = """{}
    ---
    description: {}
    consumes:
        - application/json
    produces:
        - application/json
    tags:
        - Your Project
    responses:
        200:
            description: Successful response
            schema: {}
        400:
            description: Bad Request.
            schema: CF400Schema
        404:
            description: Not Found.
            schema: CF404Schema
        422:
            description: Unprocessable Entity.
            schema: CF422Schema
        500:
            description: Internal Server Error.
            schema: CF500Schema

    """.format(
        get.__doc__, get.__doc__, name
    )

    return out


def cf_put_writer(cls, put):
    """ this is for an update only put
        200 OK is the business standard
        204 - sometimes you need to return content and this is mostly for view semantics
    """
    try:
        name = cls.response_schema.__class__.__name__
    except AttributeError as err:
        logger.debug(err)
        return put.__doc__

    res = name.lower().replace('Resource', '')

    out = """{}
    ---
    consumes:
        - application/json
    produces:
        - application/json
    tags:
        - Your Project
    responses:
        200:
            description: Successful response.
            schema: TODO
        400:
            description: Bad Request.
            schema: CF400Schema
        404:
            description: Not Found.
            schema: CF404Schema
        422:
            description: Unprocessable Entity.
            schema: CF422Schema
        500:
            description: Internal Server Error.
            schema: CF500Schema
        """.format(
        put.__doc__
    )

    return out


def cf_post_writer(cls, put):
    """ this is for an update only put
        200 OK is the business standard
        204 - sometimes you need to return content and this is mostly for view semantics
    """
    try:
        name = cls.response_schema.__class__.__name__
    except AttributeError as err:
        logger.debug(err)
        return put.__doc__

    res = name.lower().replace('Resource', '')

    out = """{}
    ---
    consumes:
         - application/json
    produces:
         - application/json
    tags:
        - Your Project
    responses:
        200:
            description: Successful response.
            schema: TODO
        400:
            description: Bad Request.
            schema: CF400Schema
        404:
            description: Not Found.
            schema: CF404Schema
        422:
            description: Unprocessable Entity.
            schema: CF422Schema
        500:
            description: Internal Server Error.
            schema: CF500Schema
        """.format(
        put.__doc__
    )

    return out


def cf_delete_writer(cls, delete):
    try:
        name = cls.response_schema.__class__.__name__
    except AttributeError as err:
        logger.debug(err)
        return delete.__doc__

    res = name.lower().replace('Resource', '')

    out = """{}
    ---
    consumes:
        - application/json
    produces:
        - application/json
    tags:
        - Your Project
    responses:
        200:
            description: Successful response.
            schema: TODO
        400:
            description: Bad Request.
            schema: CF400Schema
        404:
            description: Not Found.
            schema: CF404Schema
        422:
            description: Unprocessable Entity.
            schema: CF422Schema
        500:
            description: Internal Server Error.
            schema: CF500Schema
        """

    return out
