"""
File: structure_writer.py
Description:
    Take all of the database models and
    write out their jsonschemas. If I understand correctly, this
    is what swagger documents are made of. Along with snips and snails, ...

https://github.com/podhmo/alchemyjsonschema
"""

from alchemyjsonschema import SchemaFactory, StructuralWalker
from models import Answer, Category, Exclusion, NPCountry, Pillar, Product, Question
from pprint import pprint as pp


def create(factory):
    adict = {
        'answer': factory(Answer),
        'category': factory(Category),
        'exclusion': factory(Exclusion),
        'npcountry': factory(NPCountry),
        'pillar': factory(Pillar),
        'product': factory(Product),
        'question': factory(Question),
    }
    return adict


if __name__ == '__main__':
    factory = SchemaFactory(StructuralWalker)
    data = create(factory)

    pp(data.get('answer'))
