"""
This generates a swagger using our falcon app
"""
import json
import yaml
import logging
import os

from alchemyjsonschema import SchemaFactory, StructuralWalker, NoForeignKeyWalker
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from falcon_apispec import FalconPlugin

from rapid_raptor.app import app


from pathlib import Path


logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main(app, path, outpath):
    """ we expect there maybe some files in the same directory
    as this script, hence the path argument
    """

    header = ''
    header_fqn = os.path.join(path, 'header.yaml')
    if os.path.isfile(header_fqn):
        with open(header_fqn) as fh:
            header = yaml.safe_load(fh)

    title = header.get("info", {}).pop("title") or 'Proof of Concept'
    version = header.get("info", {}).pop("version", '1.0.0')
    openapi_version = header.pop("openapi", '2.0') or '3.0.2'

    # Create an APISpec
    spec = APISpec(
        title=title,
        version=version,
        openapi_version=openapi_version,
        plugins=[FalconPlugin(app), MarshmallowPlugin(),],
        **header,
    )

    # do security
    #  spec.components.security_scheme("api_key", api_key_scheme)

    # Register entities and paths

    extra_models = []
    for node in app._router._roots:
        for resource_node in node.children:
            resource = resource_node.resource
            try:
                schema = resource.response_schema
            except AttributeError:
                schema = None

            if schema:
                spec.components.schema(node.raw_segment.title(), schema=schema)
                request_model = schema.opts.model

                # lets try to build request params
                # this is closer to what "simple" does and is appropriate for response params
                factory = SchemaFactory(NoForeignKeyWalker)
                adict = factory(request_model)

                # XXX TODO: ugly, brittle 2019-11-09  (jwhisnant)
                really_required = set(adict.get('required')) - set(
                    ['created_at', 'updated_at', 'id']
                )
                adict['required'] = sorted(list(really_required))

                spec.path(resource=resource, parameters=adict)

            if not schema:
                # fallback to alchemyjson assuming it is "simple"
                model = resource.model
                extra_models.append(model)

    return spec, extra_models


def save(spec, path):
    """ save a json file """

    fn = os.environ.get('APP_NAME', 'python_microservice_swagger')
    json_fqn = os.path.join(path, f'{fn}.json')

    adict = spec.to_dict()
    with open(json_fqn, 'w') as fh:
        json.dump(adict, fh, indent=4)
        logger.info(f'Wrote {json_fqn}')

    data = spec.to_yaml()
    yaml_fqn = os.path.join(path, f'{fn}.yaml')
    with open(yaml_fqn, 'w') as fh:
        fh.write(data)
        logger.info(f'Wrote {yaml_fqn}')


if __name__ == '__main__':
    cur_path = Path(os.path.dirname(os.path.abspath(__file__)))
    path = cur_path.parent.joinpath('swagger')
    outpath = cur_path.parent.joinpath('swagger', 'output')

    # outpath = os.path.join(path, 'output')
    if not os.path.isdir(outpath):
        os.mkdir(outpath)

    spec, extra_models = main(app, path, outpath)
    save(spec, outpath)

    for model in extra_models:
        #  factory = SchemaFactory(StructuralWalker) # this is a verbose sort of schema ...
        # this is closer to what "simple" does and is appropriate for response params
        factory = SchemaFactory(NoForeignKeyWalker)
        adict = factory(model)

        json_fqn = os.path.join(outpath, f'{model.__name__}.json')
        with open(json_fqn, 'w') as fh:
            json.dump(adict, fh, indent=4)
            logger.info(f'Wrote {json_fqn}')
