"""
This is where we will create our data specific for each endpoint
The document strings are important for creating a swagger document.

Another python program will assemble the pieces we want into an app.

"""

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from rapid_raptor.schemas import db, models
from rapid_raptor.swagger import framework  # noqa: F401
from rapid_raptor.swagger.framework import TODO, cf_writer


class ExclusionRequestSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Exclusion
        sqla_session = db.session

        load_instance = True
        include_relationships = True


class ExclusionResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Exclusion
        sqla_session = db.session

        load_instance = True
        include_relationships = True


class ExclusionCollectionResponseSchema(ExclusionResponseSchema):
    exclusions = fields.Nested(ExclusionResponseSchema, many=True)


@cf_writer()
class ExclusionCollectionResource:
    """ multiple exclusions"""

    def on_get(self, req, resp):
        """Get exclusions """

    def on_put(self, req, resp):
        """Create an exclusion."""


@cf_writer()
class ExclusionResource:
    """ this is a falcon endpoint for a Exclusion """

    def on_get(self, req, resp):
        """Get a exclusion """

    def on_put(self, req, resp):
        """Create anexclusion. """

    def on_delete(self, req, resp):
        """Delete a exclusion."""
