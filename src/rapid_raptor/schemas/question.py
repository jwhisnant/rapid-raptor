"""
This is where we will create our data specific for each endpoint
The document strings are important for creating a swagger document.

Another python program will assemble the pieces we want into an app.

"""

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from rapid_raptor.schemas import db, models
from rapid_raptor.swagger import framework  # noqa: F401

from rapid_raptor.swagger.framework import TODO, cf_writer

from rapid_raptor.schemas.exclusion import ExclusionResponseSchema
from rapid_raptor.schemas.answer import AnswerResponseSchema


class QuestionRequestSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Question
        sqla_session = db.session
        load_instance = True
        include_relationships = True


class QuestionResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Question
        sqla_session = db.session
        load_instance = True
        include_relationships = True


class VerboseQuestionResponseSchema(QuestionResponseSchema):
    exclusions = fields.Nested(ExclusionResponseSchema, many=True,)
    answers = fields.Nested(AnswerResponseSchema, many=True,)


class QuestionCollectionResponseSchema(QuestionResponseSchema):
    categories = fields.Nested(QuestionResponseSchema, many=True)


@cf_writer()
class QuestionCollectionResource:
    """ multiple questions"""

    def on_get(self, req, resp):
        """Get questions """

    def on_put(self, req, resp):
        """Create a question."""


@cf_writer()
class QuestionResource:
    """ this is a falcon endpoint for a Question """

    def on_get(self, req, resp):
        """Get a question"""

    def on_put(self, req, resp):
        """Create a question. """

    def on_delete(self, req, resp):
        """Delete a question."""
