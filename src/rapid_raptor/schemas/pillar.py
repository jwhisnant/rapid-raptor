"""
This is where we will create our data specific for each endpoint
The document strings are important for creating a swagger document.

Another python program will assemble the pieces we want into an app.

"""
import json

from falcon import HTTPBadRequest, HTTPNotFound, HTTPUnprocessableEntity
from falcon import HTTPError

from marshmallow import ValidationError
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from schemas import db, models
from swagger.framework import TODO, cf_writer
from app.ma_resource import MaResource


class PillarResource(MaResource):
    """ this is a falcon endpoint for a pillar - we don't need any customizations from the base class"""


@cf_writer()
class NotPillarResource:
    """ this is a falcon endpoint for a pillar """

    def __init__(
        self, model, response_schema_class=None, request_schema_class=None,
    ):
        self.response_schema = response_schema_class
        self.request_schema = request_schema_class
        self.model = model

    def on_get(self, req, resp, one_item_id):
        """Get a pillar"""
        pillar = (
            self.session.query(self.model)
            .filter(self.model.id == one_item_id)
            .one_or_none()
        )

        response_schema_class = getattr(self, 'response_schema') or getattr(
            self.model, '__marshmallow__'
        )
        if not response_schema_class:
            raise ValueError(f'response_schema_class if required for func_name')

        # we need the instanace of a class ...
        response_schema = response_schema_class()

        if not pillar:
            raise HTTPNotFound()
        if pillar:
            resp.media = self.response_schema.dump(pillar)

    def on_put(self, req, resp, pillar_id):
        """
        Updates a pillar.
        If it does not exist, that is an error.
        We only update an existing pillar...
        """

        pillar = (
            self.session.query(models.Pillar)
            .filter(models.Pillar.id == pillar_id)
            .one_or_none()
        )
        if not pillar:
            raise HTTPNotFound()

        # the json we got in is stored in req.context.json
        try:
            self.request_schema.load(req.context.json, instance=pillar)

        except ValidationError as exc:
            raise HTTPUnprocessableEntity(description=json.dumps(exc.messages))

        # save our changes
        self.session.commit()

        #  what we send back - json
        #  class -->(schema.dump)--> dict --> (json.dumps) --> json
        resp.body = json.dumps(self.response_schema.dump(pillar))

    def on_post(self, req, resp, pillar_id):
        """
        Create a pillar.
        If it already exists, then we won't create it.
        We return the thing we created ...
        """

        pillar = (
            self.session.query(models.Pillar)
            .filter(models.Pillar.id == pillar_id)
            .one_or_none()
        )
        if pillar:
            raise HTTPBadRequest(
                description='Pillar exists, use PUT if you want to edit an existing pillar'
            )

        try:
            new_instance = self.response_schema.load(req.media)
            new_instance.id = pillar_id
        except ValidationError as exc:
            raise HTTPUnprocessableEntity(description=json.dumps(exc.messages))

        self.session.add(new_instance)
        self.session.commit()

        #  what we send back - json
        #  class -->(schema.dump)--> dict --> (json.dumps) --> json
        resp.body = json.dumps(self.response_schema.dump(new_instance))

    def on_delete(self, req, resp, pillar_id):
        """Delete a pillar.
        # XXX TODO: Do we really want to deactivate instead ?? 2019-12-13  (jwhisnant)

        """
        pillar = (
            self.session.query(models.Pillar)
            .filter(models.Pillar.id == pillar_id)
            .one_or_none()
        )

        if not pillar:
            raise HTTPNotFound()

        if pillar:
            db.session.delete(pillar)
            db.session.commit()
