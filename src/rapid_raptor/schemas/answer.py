"""
This is where we will create our data specific for each endpoint
The document strings are important for creating a swagger document.

Another python program will assemble the pieces we want into an app.

"""

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from rapid_raptor.schemas import db, models
from rapid_raptor.swagger.framework import TODO, cf_writer


class AnswerRequestSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Answer
        sqla_session = db.session

        load_instance = True
        include_relationships = True


class AnswerResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Answer
        sqla_session = db.session

        load_instance = True
        include_relationships = True


@cf_writer()
class AnswerCollectionResource:
    """ multiple answers"""

    # the request schema is mostly for swagger
    response_schema = AnswerResponseSchema()
    request_schema = AnswerRequestSchema()

    def on_get(self, req, resp):
        """Get answers """

    def on_put(self, req, resp):
        """Create an answer """


@cf_writer()
class AnswerResource:
    """ this is a falcon endpoint for an Answer """

    # the request schema is mostly for swagger
    response_schema = AnswerResponseSchema()
    request_schema = AnswerRequestSchema()

    def on_get(self, req, resp):
        """Get an answer """

    def on_put(self, req, resp):
        """Create an answer"""

    def on_delete(self, req, resp):
        """Delete an answer"""
