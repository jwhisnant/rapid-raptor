"""
File: db.py
Description:
    This is where we are going to put the session-making
    code so that we can consolidate it in one place

  https://eshlox.net/2017/07/28/integrate-sqlalchemy-with-falcon-framework
  engine = create_engine(
      '{engine}://{username}:{password}@{host}:{port}/{db_name}'.format(
        #  **settings.POSTGRESQL
      )
)
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.orm.session import sessionmaker


# XXX FIXME: The password for dev is literally "password" 2019-10-23  (jwhisnant)S
# XXX TODO: probably use dotenv 2019-10-23  (jwhisnant)

# XXX TODO: cross-platform 2020/05/21  ()
filepath = '/tmp/poc.db'

sqlurl = f"sqlite+pysqlite:///{filepath}"
engine = create_engine(sqlurl)

# new for the sqlalchemy middleare
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session(expire_on_commit=False)
