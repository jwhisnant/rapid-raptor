"""
This is where we will create our data specific for each endpoint
The document strings are important for creating a swagger document.

Another python program will assemble the pieces we want into an app.

"""

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from schemas import db, models
from swagger import framework  # noqa: F401
from swagger.framework import TODO, cf_writer


class CountryRequestSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Country
        sqla_session = db.session
        load_instance = True
        include_relationships = True


class CountryResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Country
        sqla_session = db.session
        load_instance = True
        include_relationships = True


class CountryCollectionResponseSchema(CountryResponseSchema):
    categories = fields.Nested(CountryResponseSchema, many=True)


@cf_writer()
class CountryCollectionResource:
    """ multiple categories"""

    def on_get(self, req, resp):
        """Get countries"""

    def on_put(self, req, resp):
        """Create a country."""


@cf_writer()
class CountryResource:
    """ this is a falcon endpoint for a Country """

    def on_get(self, req, resp):
        """Get a country"""

    def on_put(self, req, resp):
        """Create a country."""

    def on_delete(self, req, resp):
        """Delete a country."""
