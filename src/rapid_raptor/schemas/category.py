"""
This is where we will create our data specific for each endpoint
The document strings are important for creating a swagger document.

Another python program will assemble the pieces we want into an app.

"""

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

from schemas import db, models
from swagger import framework  # noqa: F401
from swagger.framework import TODO, cf_writer


class CategoryRequestSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Category
        sqla_session = db.session

        load_instance = True
        include_relationships = True


class CategoryResponseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = models.Category
        sqla_session = db.session

        load_instance = True
        include_relationships = True


class CategoryCollectionResponseSchema(CategoryResponseSchema):
    categories = fields.Nested(CategoryResponseSchema, many=True)


@cf_writer()
class CategoryCollectionResource:
    """ multiple categories. """

    def on_get(self, req, resp):
        """Get categories """

    def on_put(self, req, resp):
        """Create a category."""


@cf_writer()
class CategoryResource:
    """ this is a falcon endpoint for a Category """

    def on_get(self, req, resp):
        """Get a category"""

    def on_put(self, req, resp):
        """Create a category."""

    def on_delete(self, req, resp):
        """Delete a category. """
