"""
This is where we will put the SQLAlchemy models.
"""

import logging

# we can inherit from here and get some session checking and validation ...
from falcon_sqlalchemy.util import CreatedAtMixin, DictableMixin, UpdatedAtMixin
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
)

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_repr import RepresentableBase

from rapid_raptor.schemas.db import engine

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# this line allows for very verbose logging for our queries
# logging.getLogger('sqlalchemy.engine.base').setLevel(logging.DEBUG)
logging.getLogger('sqlalchemy.engine.base').setLevel(logging.INFO)  # some logging


# XXX TODO: clean-up some of the class hiearchies 2019-10-23  (jwhisnant)
Base = declarative_base(cls=(RepresentableBase, DictableMixin))

ItemBase = Base

AuditBase = declarative_base(cls=(ItemBase, CreatedAtMixin, UpdatedAtMixin,))


"""
Below are our data models for our classes
"""


class NPCountry(AuditBase):
    """ A country """

    # we use NPCountry as a class name because it collides with Country from Utils

    # table args
    __tablename__ = 'country'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(3), unique=True, nullable=False)

    # relationships

    pillars = relationship('Pillar', backref='country')
    products = relationship('Product', backref='country')
    categories = relationship('Category', backref='country')
    exclusions = relationship('Exclusion', backref='country')


class Product(AuditBase):
    """A product """

    # table args
    __tablename__ = 'product'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True, nullable=False)
    status_code = Column(String(255), nullable=True)
    notes = Column(String(255), nullable=True)
    is_active = Column(Integer, default=1, nullable=True)

    # relationships
    country_id = Column(Integer, ForeignKey('country.id'))


class Pillar(AuditBase):
    """A pillar - its just a name """

    # table args
    __tablename__ = 'pillar'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True, nullable=False)

    # relationships
    questions = relationship('Question', backref='pillars')
    country_id = Column(Integer, ForeignKey('country.id'))


class AnswerCategoryPoint(AuditBase):
    """Point are just an integer that a category assigns """

    # table args
    __tablename__ = 'answer_category_point'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    value = Column(Integer, nullable=False)  # this is how many points

    # relationships
    answer_id = Column(Integer, ForeignKey('answer.id'))
    category_id = Column(Integer, ForeignKey('category.id'))


class ProductCategoryPoint(AuditBase):
    """Point are just an integer that a category assigns """

    # table args
    __tablename__ = 'product_category_point'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    value = Column(Integer, unique=True, nullable=False)

    # relationships
    category_id = Column(Integer, ForeignKey('category.id'))
    product_id = Column(Integer, ForeignKey('product.id'))


class Category(AuditBase):
    """A category """

    # table args
    __tablename__ = 'category'

    # do define a unique constraint, we can do this
    __table_args__ = (
        #  UniqueConstraint('name', 'points', name='_name_points_uc'),
        {'extend_existing': True}
    )

    # fields
    id = Column(Integer, primary_key=True)
    name = Column(String(255))

    # relationships
    country_id = Column(Integer, ForeignKey('country.id'))


class Exclusion(AuditBase):
    """ An exclusion """

    # table args
    __tablename__ = 'exclusion'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True)

    # relationships
    answer_id = Column(Integer, ForeignKey('answer.id'))
    question_id = Column(Integer, ForeignKey('question.id'))
    country_id = Column(Integer, ForeignKey('country.id'))


class Answer(AuditBase):
    """answer"""

    # table args
    __tablename__ = 'answer'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    answer_content_id = Column(String(255), unique=False, nullable=False)
    answer_notes = Column(String(255), unique=False, nullable=True)

    question_id = Column(Integer, ForeignKey('question.id'))
    next_question_id = Column(Integer, ForeignKey('question.id'))

    question = relationship('Question', foreign_keys=[question_id])
    next_question = relationship('Question', foreign_keys=[next_question_id])

    # relationships
    exclusions = relationship('Exclusion', backref='answer')
    points = relationship('AnswerCategoryPoint', backref='answer')


class Question(AuditBase):
    """ A question """

    # table args
    __tablename__ = 'question'
    __table_args__ = {'extend_existing': True}

    # fields
    id = Column(Integer, primary_key=True)
    question_content_id = Column(String(255), nullable=True)
    question_notes = Column(String(255), nullable=True)

    # relationships
    answers = relationship('Answer', foreign_keys='Answer.question_id')
    exclusions = relationship('Exclusion', backref='question')

    pillar_id = Column(Integer, ForeignKey('pillar.id'))


def drop_and_create(engine):
    # delete all the things
    AuditBase.metadata.drop_all(bind=engine)

    # create all the things
    AuditBase.metadata.create_all(bind=engine)


# this is a convenience for setting up marshmallow schemas automatically
# it is a list of classes - current assumption is that they are all
# derived from the same base class (AuditBase)


all_models = (
    Answer,
    AnswerCategoryPoint,
    Category,
    Exclusion,
    NPCountry,
    Pillar,
    Product,
    ProductCategoryPoint,
    Question,
)

if __name__ == '__main__':
    drop_and_create(engine)
