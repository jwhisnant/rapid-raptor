"""
File: simple.py
Description:

    This is a simple server that automatically does the
    "correct thing" with sqlalchemy for data without relationships.

    SQLRootResources are for multiple things
    SQLItemResources are for single items

gunicorn --reload --timeout=86400 sample:app


"""

import falcon
from falcon_sqlalchemy import SQLItemResource, SQLRootResource
from sqlalchemy.ext.declarative import declarative_base

from .schemas.models import (
    Answer,
    Category,
    Exclusion,
    NPCountry,
    Pillar,
    Product,
    Question,
    engine,
)

Base = declarative_base()


class AnswerRootHandler(SQLRootResource):
    model = Answer


class AnswerItemHandler(SQLItemResource):
    model = Answer


class CategoryRootHandler(SQLRootResource):
    model = Category


class CategoryItemHandler(SQLItemResource):
    model = Category


class CountryRootHandler(SQLRootResource):
    model = NPCountry


class CountryItemHandler(SQLItemResource):
    model = NPCountry


class ExclusionRootHandler(SQLRootResource):
    model = Exclusion


class ExclusionItemHandler(SQLItemResource):
    model = Exclusion


class PillarRootHandler(SQLRootResource):
    model = Pillar


class PillarItemHandler(SQLItemResource):
    model = Pillar


class ProductRootHandler(SQLRootResource):
    model = Product


class ProductItemHandler(SQLItemResource):
    model = Product


class QuestionRootHandler(SQLRootResource):
    model = Question


class QuestionItemHandler(SQLItemResource):
    model = Question


def add_route(app, routes, engine=engine):
    """
    take an app, add some routes, return an app
    """
    resource_name, base, root_handler, item_handler = routes
    #  resource_name = "answer"
    resource_path = "/{}/{}/".format(base, resource_name)
    app.add_route(resource_path, root_handler(engine))
    app.add_route(resource_path + "{item_id}", item_handler(engine))

    return app


def create_app(app=None, base='simple', routes=None, engine=engine):
    if not app:
        app = falcon.API()

    if not routes:
        routes = (
            ('answer', 'simple', AnswerRootHandler, AnswerItemHandler),
            ('category', 'simple', CategoryRootHandler, CategoryItemHandler),
            ('country', 'simple', CountryRootHandler, CountryItemHandler),
            ('exclusion', 'simple', ExclusionRootHandler, ExclusionItemHandler),
            ('pillar', 'simple', PillarRootHandler, PillarItemHandler),
            ('question', 'simple', QuestionRootHandler, QuestionItemHandler),
        )

    for route in routes:
        app = add_route(app, route, engine)
    return app
