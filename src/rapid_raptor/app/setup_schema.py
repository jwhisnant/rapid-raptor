"""
File: setup_schema
Description:
    Setup marshmallow schemas
    We will but this here, until we find a better place
"""

import logging

from marshmallow_sqlalchemy import ModelConversionError, ModelSchema
from sqlalchemy import create_engine, event
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.orm.session import sessionmaker

#  https://eshlox.net/2017/07/28/integrate-sqlalchemy-with-falcon-framework

# XXX TODO: cross-platform 2020/05/21  ()
filepath = '/tmp/poc.db'

sqlurl = f"sqlite+pysqlite:///{filepath}"
engine = create_engine(sqlurl)

engine = create_engine(sqlurl)

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def setup_schema(session, model_classes):
    for class_ in model_classes:
        if hasattr(class_, "__tablename__"):
            if class_.__name__.endswith("Schema"):
                raise ModelConversionError(
                    "For safety, setup_schema can not be used when a"
                    "Model class ends with 'Schema'"
                )

            class Meta(object):
                model = class_
                sqla_session = session

            schema_class_name = "%sSchema" % class_.__name__

            schema_class = type(schema_class_name, (ModelSchema,), {"Meta": Meta})

            setattr(class_, "__marshmallow__", schema_class)
    logger.info('Autogenererated marshmallow schema for models ...')
