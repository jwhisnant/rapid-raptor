"""
This is a framework generic sort of resource
It handles root and item resources

SQLAlchemy middleware provides self.session which does auto-rollback

"""
import json

import falcon
from marshmallow import ValidationError

from sqlalchemy.exc import IntegrityError


def _add_msg_to_exc(exc):
    """_add_msg_to_exc
    given an exception class, make sure it has messages for
    representing errors - Marshmallow has then, SQLAlchemy
    puts them into an attribute called orig instead
    :param exc: an exception class

    returns as exception
    """

    # XXX TODO: not sure I like doing this way ... 2020-01-06  (jwhisnant)
    # IntegrityError has an orig attribute we can use, orig in an exception class
    messages = getattr(exc, 'messages', None)
    orig = getattr(exc, 'orig', None)

    if not messages:
        if orig:
            exc.messages = str(orig)
        if not orig:
            exc.messages = str(exc)
    return exc


def schema_installer(cls):
    """
    I make sure we have a serialization installed on the cclasses installed
    """
    if not getattr(cls, 'response_schema', None):
        response_schema_class = getattr(cls, 'response_schema_class', None) or getattr(
            cls.model, '__marshmallow__', None
        )
        if not response_schema_class:
            raise ValueError(f'response_schema_class is required for {cls}')

        # we need the instanace of a class ...
        cls.response_schema = response_schema_class()

    if not getattr(cls, 'request_schema', None):
        request_schema_class = getattr(cls, 'request_schema_classs', None) or getattr(
            cls.model, '__marshmallow__', None
        )
        if not request_schema_class:
            raise ValueError(f'request_schema_class is required for {cls}')

        # we need the instance of a class ...
        cls.request_schema = request_schema_class()

    return cls


# @cf_writer() - may we want to default this, or maybe the others classes should do this?
# XXX TODO: decide 2019-12-14  (jwhisnant)
class Resource:
    """ this is a falcon endpoint for a generic Resource single item"""

    def __init__(
        self, model, response_schema_class=None, request_schema_class=None,
    ):

        self.response_schema_class = response_schema_class
        self.request_schema_class = request_schema_class
        self.model = model

    def on_get(self, req, resp, id):
        """Get a single item """

        try:
            int(id)
        except ValueError:
            raise falcon.HTTPUnprocessableEntity(description=f'{id} is not an integer')

        schema_installer(self)
        one_item = (
            self.session.query(self.model).filter(self.model.id == id).one_or_none()
        )

        if not one_item:
            raise falcon.HTTPNotFound()
        if one_item:
            resp.media = self.response_schema.dump(one_item)

    def on_put(self, req, resp, id):
        """
        Updates a resource.
        If it does not exist, that is an error.
        We only update an existing pillar...
        """

        # client sent no json ...
        if not hasattr(req.context, 'json'):
            raise falcon.HTTPBadRequest(
                'Empty request body', 'A valid JSON document is required.'
            )

        schema_installer(self)

        one_item = (
            self.session.query(self.model).filter(self.model.id == id).one_or_none()
        )
        if not one_item:
            raise falcon.HTTPNotFound()

        # the json we got in is stored in req.context.json
        try:
            self.request_schema.load(req.context.json, instance=one_item)

        except ValidationError as exc:
            exc = _add_msg_to_exc(exc)

            raise falcon.HTTPUnprocessableEntity(description=exc.messages)

        # save our changes
        try:
            self.session.commit()
        except (IntegrityError,) as exc:
            exc = _add_msg_to_exc(exc)

            raise falcon.HTTPUnprocessableEntity(description=exc.messages)

        #  what we send back - json
        #  class -->(schema.dump)--> dict --> (json.dumps) --> json
        resp.body = json.dumps(self.response_schema.dump(one_item))

    def on_post(self, req, resp, id):
        """
        Create an item
        If it already exists, then we won't create it.
        We return the thing we created ...
        We allow client to set the id of the thing they want to create
        """

        # client sent no json ...
        if not hasattr(req.context, 'json'):
            raise falcon.HTTPBadRequest(
                'Empty request body', 'A valid JSON document is required.'
            )

        schema_installer(self)
        one_item = (
            self.session.query(self.model).filter(self.model.id == id).one_or_none()
        )

        # if it exists that is an error
        # we can debate the http status code
        # https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists#3826024
        if one_item:
            description = 'Item exists. Use PUT if you want to edit an existing item'

            raise falcon.HTTPUnprocessableEntity(description=description,)

        # invalid request schema?
        try:
            new_instance = self.response_schema.load(req.context.get('json', None))
            new_instance.id = id
        except ValidationError as exc:
            exc = _add_msg_to_exc(exc)

            raise falcon.HTTPUnprocessableEntity(
                #  description=json.dumps(exc.messages)
                description=exc.messages,
            )

        try:
            self.session.add(new_instance)
        except Exception as exc:
            exc = _add_msg_to_exc(exc)
            raise falcon.HTTPUnprocessableEntity(description=exc.messages,)

        try:
            self.session.commit()
        except Exception as exc:
            exc = _add_msg_to_exc(exc)
            raise falcon.HTTPUnprocessableEntity(description=exc.messages,)

        #  what we send back - the json of the created thing ...
        #  class -->(schema.dump)--> dict --> (json.dumps) --> json
        resp.body = json.dumps(self.response_schema.dump(new_instance))

    def on_delete(self, req, resp, id):
        """Delete a one_item.
        # XXX TODO: Do we really want to deactivate instead ?? 2019-12-13  (jwhisnant)
        """

        schema_installer(self)
        one_item = (
            self.session.query(self.model).filter(self.model.id == id).one_or_none()
        )

        # doesn't exist? NotFound
        if not one_item:
            raise falcon.HTTPNotFound()

        if one_item:
            # XXX TODO: 409 conflict 2019-12-14  (jwhisnant)
            self.session.delete(one_item)
            self.session.commit()


class Collection:
    """ this is a falcon endpoint for a generic Resource single item"""

    def __init__(
        self, model, response_schema_class=None, request_schema_class=None,
    ):

        self.response_schema_class = response_schema_class
        self.request_schema_class = request_schema_class
        self.model = model

    def on_get(self, req, resp):
        schema_installer(self)

        # XXX TODO: if we got params, are they ok??? 2019-12-18  (jwhisnant)
        if req.params:
            try:
                self.request_schema.validate(req.params)
            except ValidationError as exc:
                exc = _add_msg_to_exc(exc)
                raise falcon.HTTPUnprocessableEntity(description=exc.messages,)

        query = self.session.query(self.model)
        for key, value in req.params.items():
            # these must exist, because they were caught in validation above if they weren't valid
            mod = getattr(self.model, key)
            query = query.filter(mod == value)

        # we are sending multiple things back...
        self.response_schema.many = True

        resp.media = self.response_schema.dump(query.all())

    def on_put(self, req, resp, id=None):
        """ raise 405 """
        raise falcon.HTTPMethodNotAllowed(allowed_methods=['GET', 'OPTIONS'])

    def on_post(self, req, resp, id=None):
        """ raise 405 """
        raise falcon.HTTPMethodNotAllowed(allowed_methods=['GET', 'OPTIONS'])

    def on_delete(self, req, resp, id=None):
        """ raise 405 """
        raise falcon.HTTPMethodNotAllowed(allowed_methods=['GET', 'OPTIONS'])
