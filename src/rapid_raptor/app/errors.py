"""
we have custom responses for all http errors, so lets catch HTTPErrors
"""
import falcon
from datetime import datetime
from falcon import HTTPBadRequest, HTTPNotFound, HTTPUnprocessableEntity, HTTPError


def _parse_meta(adict, key, where='meta'):
    return adict.get(key) or adict.get(where, {}).get(key) or None


def convert_to_cf(adict):
    """ convert to a cf error response """
    raise NotImplementedError


def convert_to_uv(adict):
    """ convert to a uv error response """

    return adict  # XXX FIXME: desc 2019-12-14  (jwhisnant)

    # XXX FIXME: implement 2019-11-09  (jwhisnant)
    curr_date_time = datetime.now()
    template = {
        "correlation_id": "None",
        "errors": [
            {
                "cause": adict.get('cause', 'None'),
                "code": adict.get('code') or adict.get('title'),
                "message": adict.get('message', 'None'),
                "status": adict.get('status') or adict.get('title'),
            }
        ],
        "meta": {
            # str(getattr(thread_local, 'correlation_id', '00000000'))[:8]
            "svrCtrlCode": _parse_meta(adict, "svrCtrlCode") or 'FIXME',
            "svrCtrlName": _parse_meta(adict, "svrCtrlName") or "errSystem",
            "svrLsDate": _parse_meta(adict, "svrLsDate")
            or curr_date_time.strftime('%m/%d/%Y'),
            "svrLsTime": _parse_meta(adict, "svrLsTime")
            or curr_date_time.strftime('%I:%M:%S%p').lower(),
            "svrMessage": "Internal Server Error [Invalid error response structure received.  Default structure has been used.  Origin of error should be investigated.]",
            "svrStatus": -1,
        },
    }

    return template


def uv_serializer(req, resp, exception):
    """ uv specific error messages """
    return generic_serializer(req, resp, exception, func=convert_to_uv)


def generic_serializer(req, resp, exception, func=None):
    """ raise a UV formatted HTTPError 
    https://falcon.readthedocs.io/en/stable/api/api.html#falcon.API.set_error_serializer
    """

    representation = None

    preferred = req.client_prefers(('application/x-yaml', 'application/json'))

    # 404 does not has_representation - keep this in mind
    if exception.has_representation and preferred is not None and func:
        if preferred == 'application/json':
            representation = func(exception.to_dict())
        else:
            representation = yaml.dump(exception.to_dict(), encoding=None)
        resp.media = representation
        resp.content_type = preferred

    resp.append_header('Vary', 'Accept')
