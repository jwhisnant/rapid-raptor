"""
File: middleware.py
Description:
    This is where we are going to put the session-making
    code so that we can consolidate it in one place
"""

import os
import logging

from marshmallow_sqlalchemy import ModelConversionError, ModelSchema
from sqlalchemy import create_engine, event
from sqlalchemy.event import listens_for
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.pool import Pool

from rapid_raptor.schemas.models import drop_and_create

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def create_session():
    filepath = os.environ.get('DB_FILE', '/tmp/poc.db')

    #  https://eshlox.net/2017/07/28/integrate-sqlalchemy-with-falcon-framework
    # XXX TODO: cross-platform 2020/05/21  ()

    sqlurl = f"sqlite+pysqlite:///{filepath}"
    engine = create_engine(sqlurl)

    if not os.path.isfile(filepath):
        logger.info(f'Creating empty tables using {engine}')

        drop_and_create(engine)

    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)

    logger.info(f'Using database file {filepath}')

    return Session


class SQLAlchemySessionManager:
    """
    Create a scoped session for every request and close it when the request
    ends.
    """

    def __init__(self, Session):
        self.Session = Session

    def process_resource(self, req, resp, resource, params):
        resource.session = self.Session()

    def process_response(self, req, resp, resource, req_succeeded):
        if hasattr(resource, 'session'):
            if not req_succeeded:
                resource.session.rollback()
            self.Session.remove()


class CorrelationIdAdder:
    def process_request(self, req, resp):
        """ first thing, stick a correlation id in req.context """
