"""
This is where we will create our falcon API which we will name app
#  https://eshlox.net/2017/07/28/integrate-sqlalchemy-with-falcon-framework

"""
import falcon

from falcon_marshmallow import Marshmallow

from rapid_raptor.app.resources import Resource, Collection

from rapid_raptor.app.middleware import CorrelationIdAdder, SQLAlchemySessionManager, create_session

from rapid_raptor.app.setup_schema import setup_schema

from rapid_raptor.schemas.question import VerboseQuestionResponseSchema
from rapid_raptor.schemas import models
from rapid_raptor.schemas.models import all_models

from rapid_raptor.swagger.framework import cf_writer


@cf_writer()
class PillarCollectionResource(Collection):
    """ this is a falcon endpoint for a pillar - no customizations"""


@cf_writer()
class CountryCollectionResource(Collection):
    """ this is a falcon endpoint for a country - no customizations"""


@cf_writer()
class CategoryCollectionResource(Collection):
    """ this is a falcon endpoint for a category - no customizations"""


@cf_writer()
class ExclusionCollectionResource(Collection):
    """ this is a falcon endpoint for an exclusion - no customizations"""


@cf_writer()
class QuestionCollectionResource(Collection):
    """ this is a falcon endpoint for a question - no customizations"""


@cf_writer()
class ProductCollectionResource(Collection):
    """ this is a falcon endpoint for an product - no customizations"""


@cf_writer()
class AnswerCollectionResource(Collection):
    """ this is a falcon endpoint for an answer - no customizations"""


@cf_writer()
class VerboseQuestionCollectionResource(Collection):
    """ this is a falcon endpoint for a verbose json of a question - we want to show some verbose sub-items """

    # XXX TODO: adding response classes could be more straightforward 2019-12-14  (jwhisnant)
    def __init__(self, model, request_schema_class=None, response_schema_class=None):
        super().__init__(model, request_schema_class, response_schema_class)
        self.response_schema_class = VerboseQuestionResponseSchema


# items


@cf_writer()
class PillarResource(Resource):
    """ this is a falcon endpoint for a pillar - no customizations"""


@cf_writer()
class CountryResource(Resource):
    """ this is a falcon endpoint for a countery - no customizations"""


@cf_writer()
class CategoryResource(Resource):
    """ this is a falcon endpoint for a category - no customizations"""


@cf_writer()
class ExclusionResource(Resource):
    """ this is a falcon endpoint for an exclusion - no customizations"""


@cf_writer()
class QuestionResource(Resource):
    """ this is a falcon endpoint for a question - no customizations"""


@cf_writer()
class ProductResource(Resource):
    """ this is a falcon endpoint for an product - no customizations"""

    def on_delete(self, req, resp, id):
        """ raise 405 """
        raise falcon.HTTPMethodNotAllowed(
            allowed_methods=['GET', 'POST', 'PUT', 'OPTIONS']
        )


@cf_writer()
class AnswerResource(Resource):
    """ this is a falcon endpoint for an answer - no customizations"""


@cf_writer()
class VerboseQuestionResource(Resource):
    """ this is a falcon endpoint for a verbose json of a question - we want to show some verbose sub-items """

    # XXX TODO: adding response classes could be more straightforward 2019-12-14  (jwhisnant)
    def __init__(self, model, request_schema_class=None, response_schema_class=None):
        super().__init__(model, request_schema_class, response_schema_class)
        self.response_schema_class = VerboseQuestionResponseSchema


def add_routes(app, routes=None):
    if not routes:
        # the name of the parameter matters to the Resource class. So always use {id}

        items = [
            ('/answers/{id}', AnswerResource(model=models.Answer)),
            ('/categories/{id}', CategoryResource(model=models.Category)),
            ('/countries/{id}', CountryResource(model=models.NPCountry)),
            ('/exclusions/{id}', ExclusionResource(model=models.Exclusion)),
            ('/pillars/{id}', PillarResource(model=models.Pillar)),
            ('/products/{id}', ProductResource(model=models.Product)),
            ('/questions/{id}', QuestionResource(model=models.Question)),
            ('/verbose_questions/{id}', VerboseQuestionResource(model=models.Question)),
        ]

        roots = [
            ('/answers/', AnswerCollectionResource(model=models.Answer)),
            ('/categories/', CategoryCollectionResource(model=models.Category)),
            ('/countries/', CountryCollectionResource(model=models.NPCountry)),
            ('/exclusions/', ExclusionCollectionResource(model=models.Exclusion)),
            ('/pillars/', PillarCollectionResource(model=models.Pillar)),
            ('/products/', ProductCollectionResource(model=models.Product)),
            ('/questions/', QuestionCollectionResource(model=models.Question)),
            (
                '/verbose_questions/',
                VerboseQuestionCollectionResource(model=models.Question),
            ),
        ]

        routes = items + roots

    for name, resource in routes:
        app.add_route(name, resource)

    return app


def create_app(app=None):
    # create instance of resource
    # pass into `add_route` for Falcon
    if not app:
        Session = create_session()
        setup_schema(Session, all_models)

        app = falcon.API(
            middleware=[
                CorrelationIdAdder(),
                SQLAlchemySessionManager(Session),
                Marshmallow(),
            ]
        )
    app = add_routes(app)

    return app


app = create_app()
