# logging
import logging
from wsgiref import simple_server

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


if __name__ == '__main__':
    from app import app

    httpd = simple_server.make_server('', 8080, app)
    logger.debug('Server started.')
    httpd.serve_forever()
