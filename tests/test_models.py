"""
This is to test creation of some models into a database with sqlalchemy
"""

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker

import context  # this updates our paths for tests

from question import VerboseQuestionResponseSchema, QuestionResponseSchema

from schemas.models import (
    Answer,
    AnswerCategoryPoint,
    Category,
    Exclusion,
    NPCountry,
    Pillar,
    Product,
    ProductCategoryPoint,
    Question,
    drop_and_create,
)


@pytest.fixture(scope='session')
def session():
    """ create in memory database session"""
    # only do this once for our tests
    # XXX TODO: probably use dotenv 2019-10-23  (jwhisnant)
    sqlurl = 'sqlite://'  # this for testing

    engine = create_engine(sqlurl, echo=True)

    Session = sessionmaker(bind=engine, autoflush=False)
    session = Session(expire_on_commit=False)

    connection = engine.connect()
    connection.close()

    drop_and_create(engine)

    return session


@pytest.fixture(scope='session')
def answer1(session, exclusion):
    """ test can create an answer """
    answer1 = Answer(
        answer_content_id='answer_content_fixture',
        answer_notes='anwer_notes_fixture',
        exclusions=[exclusion,],
    )
    session.add(answer1)
    session.commit()
    return answer1


@pytest.fixture(scope='session')
def answer2(session):
    """ test can create second answer """
    answer2 = Answer(
        answer_content_id='answer_content_2',
        answer_notes='anwer_notes_2',
        exclusions=[Exclusion(name='fixture_ex_2')],
    )
    session.add(answer2)
    session.commit()

    return answer2


@pytest.fixture(scope='session')
def question2(session):
    """ create a question """

    answer3 = Answer(answer_content_id='answer3')
    answer4 = Answer(answer_content_id='answer4')

    session.add(answer3)
    session.add(answer4)
    session.commit()

    data = {
        'answers': [answer3, answer4],
        'question_content_id': 'question2',
    }

    question = Question(**data)
    session.add(question)
    session.commit()

    return question


@pytest.fixture(scope='session')
def category(session):
    """ create a minimal category """
    category = Category(name='bar')
    session.add(category)
    session.commit()

    return category


@pytest.fixture(scope='session')
def exclusion(session):
    """ create a minimal exclusion """
    exclusion = Exclusion(name='exclusion1')
    session.add(exclusion)
    session.commit()
    return exclusion


@pytest.fixture(scope='session')
def country(session):
    """ create a country """
    country = NPCountry(name='GB')
    session.add(country)
    session.commit()
    return country


@pytest.fixture(scope='session')
def pillar(session):
    """ create a pillar """
    pillar = Pillar(name='Pillar 0')
    session.add(pillar)
    session.commit()

    return pillar


@pytest.fixture(scope='session')
def product(session):
    """ create a product """
    product = Product(name='product_name', notes='notes', status_code='42')
    session.add(product)
    session.commit()

    return product


@pytest.fixture(scope='session')
def answerpoint(session):
    """ create a minimal point """
    point = AnswerCategoryPoint(value=0)
    session.add(point)
    session.commit()

    return point


@pytest.fixture(scope='session')
def categorypoint(session):
    """ create a minimal point """
    point = ProductCategoryPoint(value=0)
    session.add(point)
    session.commit()

    return point


@pytest.fixture(scope='session')
def question1(session, answer1, answer2, exclusion):
    """ create a question """
    data = {
        'question_content_id': 'question_content_id',
        'question_notes': 'How are you?',
        'answers': [answer1, answer2],
        'exclusions': [exclusion,],
    }

    question = Question(**data)
    session.add(question)
    session.commit()

    return question


@pytest.mark.run(order=0)
def test_can_create_category(session, categorypoint):
    """ create a very basic category
    that has some points
    """

    # no categories in table
    any_categories = session.query(Category).all()
    assert any_categories == list()

    # add 'foo'
    category = Category(name='foo')
    session.add(category)
    session.commit()

    # assert 'foo' now in table
    foo = session.query(Category).filter(Category.name == 'foo').one()
    assert foo.name == 'foo'

    return category


@pytest.mark.run(order=1)
def test_can_create_question(session, answer1, answer2, exclusion):
    """ create a question, a very complex structure"""

    any_question = session.query(Question).all()
    assert any_question == list()

    #  category = Category(name='category answer 1',)

    answer1 = Answer(
        answer_content_id='answer_content_1',
        answer_notes='anwer_notes_1',
        #  categories=[category],
        exclusions=[exclusion],
        points=[AnswerCategoryPoint(value=1), AnswerCategoryPoint(value=2)],
    )

    session.add(answer1)
    session.commit()

    #  category = Category(name='category answer 2')
    answer2 = Answer(
        answer_content_id='answer_content_2',
        answer_notes='anwer_notes_2',
        #  categories=[category, Category(name='bar')],
        exclusions=[Exclusion(name='exclusion2')],
        points=[AnswerCategoryPoint(value=4), AnswerCategoryPoint(value=5)],
    )

    session.add(answer2)
    session.commit()

    data = {
        'question_content_id': 'question_content_id',
        'question_notes': 'How are you?',
        'answers': [answer1, answer2],
        'exclusions': [exclusion,],
    }

    question = Question(**data)
    session.add(question)
    session.commit()

    # assert question now in table ...
    question = session.query(Question).one()
    assert question.question_content_id == 'question_content_id'
    assert question.id == 1
    assert answer1 in question.answers


@pytest.mark.run(order=3)
def test_can_update_country(session, country, category, exclusion, pillar, product):
    """ update a country with more data ... this is an example to show
    how updates might work ...
    """

    assert country in session
    assert country.categories == []
    assert country.exclusions == []
    assert country.pillars == []
    assert country.products == []

    country.categories.append(category)
    country.exclusions.append(exclusion)
    country.pillars.append(pillar)
    country.products.append(product)

    # use merge, but add would probably work also
    session.merge(country)
    session.commit()

    # show it really was saved?
    record = session.query(NPCountry).filter(NPCountry.id == country.id).one()
    assert category in record.categories
    assert exclusion in record.exclusions
    assert pillar in record.pillars
    assert product in record.products


@pytest.mark.run(order=4)
def test_question_schema(session):
    """ test deserializing - that is what we might give back as a response """
    record = session.query(Question).filter(Question.id == 1).one()
    schema = QuestionResponseSchema()

    data = schema.dump(record)

    expected = {
        'answers': [3, 4],
        'created_at': '2019-10-25T13:57:26.854481',
        'exclusions': [1],
        'pillars': None,
        'id': 1,
        'question_content_id': 'question_content_id',
        'question_notes': 'How are you?',
        'updated_at': '2019-10-25T13:57:26.854481',
    }

    # remove variable data
    for key in ('created_at', 'updated_at'):
        for adict in (data, expected):
            del adict[key]

    assert data == expected


@pytest.mark.run(order=5)
def test_verbose_question_schema(session):
    """
    test deserializing - that is what we might give back as a response - verbose
    by default subobjects are just referenced by their id number
    but this shows how we can be verbose ...
    """

    record = session.query(Question).filter(Question.id == 1).one()
    schema = VerboseQuestionResponseSchema()

    data = schema.dump(record)
    expected = {
        'answers': [
            {
                'answer_content_id': 'answer_content_1',
                'answer_notes': 'anwer_notes_1',
                'created_at': '2019-10-30T18:42:13.937137',
                'exclusions': [1],
                'id': 3,
                'next_question': None,
                'points': [1, 2],
                'question': 1,
                'updated_at': '2019-10-30T18:42:13.959390',
            },
            {
                'answer_content_id': 'answer_content_2',
                'answer_notes': 'anwer_notes_2',
                'created_at': '2019-10-30T18:42:13.950833',
                'exclusions': [3],
                'id': 4,
                'next_question': None,
                'points': [3, 4],
                'question': 1,
                'updated_at': '2019-10-30T18:42:13.959390',
            },
        ],
        'created_at': '2019-10-30T18:42:13.959390',
        'exclusions': [
            {
                'answer': 3,
                'country': 1,
                'created_at': '2019-10-30T18:42:13.906818',
                'id': 1,
                'name': 'exclusion1',
                'question': 1,
                'updated_at': '2019-10-30T18:42:14.002348',
            }
        ],
        'id': 1,
        'pillars': None,
        'question_content_id': 'question_content_id',
        'question_notes': 'How are you?',
        'updated_at': '2019-10-30T18:42:13.959390',
    }

    # remove variable data
    for key in ('created_at', 'updated_at'):
        for adict in (data, expected):
            del adict[key]
            for sub in ('answers', 'exclusions'):
                for item in adict.get(sub, {}):
                    del item[key]

    assert data == expected
