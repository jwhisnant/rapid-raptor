"""
https://docs.python-guide.org/writing/structure/

Add some items to the python path for tests ...

"""

import os
import sys
import pathlib

root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
root = pathlib.Path(root_dir)

base = root.joinpath('src', 'rapid_raptor')


for adir in (
    'app',
    'schemas',
    'swagger',
):

    to_add = base.joinpath(adir)
    sys.path.insert(0, str(to_add))

sys.path.insert(0, str(base))

# fmt: off
from schemas import models # nopep8
from schemas import db # nopep8
# fmt: on
