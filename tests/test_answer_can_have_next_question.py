"""
This is to test creation of some models into a database with sqlalchemy
"""

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker

#  from minimal_model import Question, Answer, drop_and_create
import context
from schemas.models import Question, Answer, drop_and_create

# logging
import logging

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.getLogger('sqlalchemy.engine.base').setLevel(logging.DEBUG)


@pytest.fixture(scope='session')
def session():
    """ create in memory database session"""
    filepath = '/tmp/test_fixture.db'

    sqlurl = f"sqlite+pysqlite:///{filepath}"
    engine = create_engine(sqlurl, echo=True)

    Session = sessionmaker(bind=engine, autoflush=False)
    session = Session(expire_on_commit=False)

    connection = engine.connect()
    connection.close()
    drop_and_create(engine)

    return session


@pytest.fixture(scope='module')
def question1(session):
    """ create a question """

    answer1 = Answer(answer_content_id='answer1')
    answer2 = Answer(answer_content_id='answer2')

    session.add(answer1)
    session.add(answer2)
    session.commit()

    data = {
        'answers': [answer1, answer2],
        'question_content_id': 'question1',
    }

    question = Question(**data)
    session.add(question)
    session.commit()
    return question


@pytest.fixture(scope='module')
def question2(session):
    """ create a question """

    answer3 = Answer(answer_content_id='answer3')
    answer4 = Answer(answer_content_id='answer4')

    session.add(answer3)
    session.add(answer4)
    session.commit()

    data = {
        'answers': [answer3, answer4],
        'question_content_id': 'question2',
    }

    question = Question(**data)
    session.add(question)
    session.commit()
    return question


def test_answer_can_have_next_question(session, question1, question2):

    assert question1 != question2
    assert question1 in session
    assert question2 in session

    question1.answers[0].next_question = question2

    # just commit the session since all items are here
    session.commit()
    after = session.query(Question).filter(Question.id == question1.id).one()

    found = 0
    for answer in after.answers:
        if answer.next_question == question2:
            found = 1

    assert found
