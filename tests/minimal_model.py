"""
This is a minimal model, which demonstrates foreign_keys ...
"""
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_repr import RepresentableBase


Base = declarative_base(cls=RepresentableBase)


class Answer(Base):
    """ this is tricky - see the first example on webpage below
    https://docs-sqlalchemy.readthedocs.io/ko/latest/orm/join_conditions.html
    """

    __tablename__ = 'answer'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)

    question_id = Column(Integer, ForeignKey('question.id'))
    next_question_id = Column(Integer, ForeignKey('question.id'))

    question = relationship('Question', foreign_keys=[question_id])
    next_question = relationship('Question', foreign_keys=[next_question_id])


class Question(Base):

    __tablename__ = 'question'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    answers = relationship('Answer', foreign_keys='Answer.question_id')


def drop_and_create(engine):
    # delete all the things
    Base.metadata.drop_all(engine)

    # create all the things
    Base.metadata.create_all(engine)
