========
Overview
========

A proof of concept using SQLAlchemy, falcon and marshmallow.


Installation
============

You install the in-development version with::

    git clone https://jwhisnant@bitbucket.org/jwhisnant/rapid-raptor.git
    pip install .


Development
===========

To run the all tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox


Demo the server
===============

To run the all tests run.

.. code-block:: shell

    tox

Running the tests will create an sqlite3 database with more interesting data.

Run gunicorn server
-------------------

.. code-block:: shell

    sh serve.sh

Test Responses
--------------

In a separate window we can see some of the responses:

.. code-block:: shell

    # normal default serialized response
    http GET http://127.0.0.1:8080/questions

    # response with expanded references
    http GET http://127.0.0.1:8080/verbose_questions

