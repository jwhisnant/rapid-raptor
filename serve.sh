# convenience to run gunicorn ...

# this assumes that you have run the tests
# this is the more "interesting" case for examples ...
export DB_FILE="/tmp/test_fixture.db"

# this is an empty data file - It will be generated if it does not exist
#export DB_FILE="/tmp/poc.db"

cd ./src/rapid_raptor/app

# for reloading and debugging
gunicorn -t 86400 --reload --reload-engine=inotify --log-level=DEBUG -b ':8080' app:app

# for reals
#gunicorn -t 86400 --log-level=INFO -b ':8080' app:app

cd -

